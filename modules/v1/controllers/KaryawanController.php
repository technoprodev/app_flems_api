<?php
namespace app_flems_api\modules\v1\controllers;

use Yii;
use app_flems\models\Karyawan;
use app_flems\models\Pesanan;
use app_flems\models\PesananPenumpang;
use app_flems\models\PesananTujuan;
use technosmart\yii\rest\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class KaryawanController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // Bearer Auth checks for Authorize: Bearer <Token> header to login the user
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::find()
            ->select(['pesanan.*', 'unit_kerja.keberangkatan_latitude', 'unit_kerja.keberangkatan_longitude'])
            ->leftJoin('unit_kerja', '`pesanan`.`id_pool` = `unit_kerja`.`id`')
            ->where(['pesanan.id' => $id])
            ->with(['penumpang', 'pool', 'pesananPenumpangs', 'pesananTujuans', 'supir'])
            ->asArray()
            ->one()) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    protected function findModelPesananPenumpang($id)
    {
        if (($model = PesananPenumpang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    protected function findModelPesananTujuan($id)
    {
        if (($model = PesananTujuan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    public function actionIndex()
    {
        $error = true;
        $errorMessage = [];

        $model['pesanan'] = new Pesanan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);
            if (isset($post['PesananTujuan'])) {
                foreach ($post['PesananTujuan'] as $key => $value) {
                    if (isset($value['id']) && $value['id'] > 0) {
                        $pesananTujuan = $this->findModelPesananTujuan($value['id']);
                        $pesananTujuan->setAttributes($value);
                    } else if (isset($value['id']) && $value['id'] < 0) {
                        $pesananTujuan = $this->findModelPesananTujuan(($value['id']*-1));
                        $pesananTujuan->isDeleted = true;
                    } else {
                        $pesananTujuan = new PesananTujuan();
                        $pesananTujuan->setAttributes($value);
                    }
                    $model['pesanan_tujuan'][] = $pesananTujuan;
                }
            }
            if (isset($post['PesananPenumpang'])) {
                foreach ($post['PesananPenumpang'] as $key => $value) {
                    if (isset($value['id']) && $value['id'] > 0) {
                        $pesananPenumpang = $this->findModelPesananPenumpang($value['id']);
                        $pesananPenumpang->setAttributes($value);
                    } else if (isset($value['id']) && $value['id'] < 0) {
                        $pesananPenumpang = $this->findModelPesananPenumpang(($value['id']*-1));
                        $pesananPenumpang->isDeleted = true;
                    } else {
                        $pesananPenumpang = new PesananPenumpang();
                        $pesananPenumpang->setAttributes($value);
                    }
                    $model['pesanan_penumpang'][] = $pesananPenumpang;
                }
            }

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                if ($model['pesanan']->isNewRecord) {
                    $model['pesanan']->createPesanan();
                }
                if (!$model['pesanan']->save()) {
                    $errorMessage = array_merge($errorMessage, $model['pesanan']->errors);
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }

                $error = false;

                if (isset($model['pesanan_tujuan']) and is_array($model['pesanan_tujuan'])) {
                    foreach ($model['pesanan_tujuan'] as $key => $pesananTujuan) {
                        $pesananTujuan->id_pesanan = $model['pesanan']->id;
                        if (!$pesananTujuan->isDeleted && !$pesananTujuan->validate()) $error = true;                        
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }
                
                    foreach ($model['pesanan_tujuan'] as $key => $pesananTujuan) {
                        if ($pesananTujuan->isDeleted) {
                            if (!$pesananTujuan->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pesananTujuan->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['pesanan_penumpang']) and is_array($model['pesanan_penumpang'])) {
                    foreach ($model['pesanan_penumpang'] as $key => $pesananPenumpang) {
                        $pesananPenumpang->id_pesanan = $model['pesanan']->id;
                        if (!$pesananPenumpang->isDeleted && !$pesananPenumpang->validate()) $error = true;                        
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                    }
                
                    foreach ($model['pesanan_penumpang'] as $key => $pesananPenumpang) {
                        if ($pesananPenumpang->isDeleted) {
                            if (!$pesananPenumpang->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pesananPenumpang->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Terjadi kesalahan pada pengisian formulir. Harap perbaiki sebelum submit ulang.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
                // if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;
        }

        if ($error)
            if ($errorMessage)
                return [
                    'code' => 400,
                    'message' => 'Validation Failed',
                    'errors' => $errorMessage,
                ];
            else
                return [
                    'code' => 500,
                    'message' => 'Internal Server Error',
                    'description' => 'We\'ve faced a problem creating the pesanan, please contact us for assistance.',
                ];
        else
            return [
                'code' => 200,
                'message' => 'Data Inserted',
                'description' => 'Pesanan created successfully',
            ];
    }

    public function actionListPenumpang()
    {
        $penumpang = \Yii::$app->db->createCommand(
            '
                SELECT u.id, u.name, k.nik, u.email, u.phone
                FROM user u
                JOIN karyawan k ON k.id_user = u.id
                WHERE u.id <> :id
            ',
            [
                'id' => Yii::$app->user->identity->id,
            ]
        )->queryAll();

        if ($penumpang) {
            return [
                'code' => 200,
                'message' => 'Data Found',
                'data' => [
                    'Karyawan' => $penumpang,
                ],
            ];
        } else {
            return [
                'code' => 400,
                'message' => 'Data Not Found',
            ];
        }
    }

    public function actionListDalamProses()
    {
        $dalamProses = Pesanan::find()
            ->select(['pesanan.*', 'unit_kerja.keberangkatan_latitude', 'unit_kerja.keberangkatan_longitude'])
            ->leftJoin('unit_kerja', '`pesanan`.`id_pool` = `unit_kerja`.`id`')
            ->where(['status' => ['Diminta','Disetujui Manager','Dialokasikan Dispatcherf','Disetujui Supervisor','SPK Telah Siap','Checkin','Checkout'], 'id_penumpang' => Yii::$app->user->identity->id])
            ->orderBy(['id' => SORT_DESC])
            ->with(['penumpang', 'pesananPenumpangs', 'pesananTujuans', 'supir'])
            ->asArray()
            ->all();

        $pesanans = [];
        foreach ($dalamProses as $key => $pesanan) {
            if ($pesanan) {
                unset($pesanan['penumpang']['password_hash']);
                unset($pesanan['penumpang']['karyawan']['id_user']);
                $pesanans[$key] = $pesanan;
                foreach ($pesanans[$key]['pesananPenumpangs'] as $key1 => $pesananPenumpang) {
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['password_hash']);
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['karyawan']['id_user']);
                }
            }
        }

        if ($pesanans) {
            return [
                'code' => 200,
                'message' => 'Data Found',
                'data' => [
                    'Pesanan' => $pesanans,
                ],
            ];
        } else {
            return [
                'code' => 404,
                'message' => 'Data Not Found',
            ];
        }
    }

    public function actionListSelesai()
    {
        $selesai = Pesanan::find()
            ->select(['pesanan.*', 'unit_kerja.keberangkatan_latitude', 'unit_kerja.keberangkatan_longitude'])
            ->leftJoin('unit_kerja', '`pesanan`.`id_pool` = `unit_kerja`.`id`')
            ->where(['status' => ['Ditolak Manager','Ditolak Supervisor','SPK Selesai'], 'id_penumpang' => Yii::$app->user->identity->id])
            ->orderBy(['id' => SORT_DESC])
            ->with(['penumpang', 'pesananPenumpangs', 'pesananTujuans', 'supir'])
            ->asArray()
            ->all();

        $pesanans = [];
        foreach ($selesai as $key => $pesanan) {
            if ($pesanan) {
                unset($pesanan['penumpang']['password_hash']);
                unset($pesanan['penumpang']['karyawan']['id_user']);
                $pesanans[$key] = $pesanan;
                foreach ($pesanans[$key]['pesananPenumpangs'] as $key1 => $pesananPenumpang) {
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['password_hash']);
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['karyawan']['id_user']);
                }
            }
        }

        if ($pesanans) {
            return [
                'code' => 200,
                'message' => 'Data Found',
                'data' => [
                    'Pesanan' => $pesanans,
                ],
            ];
        } else {
            return [
                'code' => 404,
                'message' => 'Data Not Found',
            ];
        }
    }

    public function actionDetail($id)
    {
        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        if ($model['pesanan']) {
            return [
                'code' => 200,
                'message' => 'Data Found',
                'data' => [
                    'Pesanan' => $model['pesanan'],
                ],
            ];
        } else {
            return [
                'code' => 400,
                'message' => 'Data Not Found',
            ];
        }
    }
}
