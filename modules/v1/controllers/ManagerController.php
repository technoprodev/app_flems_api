<?php
namespace app_flems_api\modules\v1\controllers;

use Yii;
use app_flems\models\Karyawan;
use app_flems\models\Pesanan;
use app_flems\models\PesananPenumpang;
use app_flems\models\PesananTujuan;
use technosmart\yii\rest\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class ManagerController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // Bearer Auth checks for Authorize: Bearer <Token> header to login the user
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    protected function findModel($id)
    {
        if (($model = Pesanan::find()->select(['pesanan.*', 'unit_kerja.keberangkatan_latitude', 'unit_kerja.keberangkatan_longitude'])->leftJoin('unit_kerja', '`pesanan`.`id_unit_kerja` = `unit_kerja`.`id`')->where(['pesanan.id' => $id])->with(['penumpang', 'unitKerja', 'pesananPenumpangs', 'pesananTujuans', 'supir'])->one()) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    protected function findModelPesananPenumpang($id)
    {
        if (($model = PesananPenumpang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    protected function findModelPesananTujuan($id)
    {
        if (($model = PesananTujuan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    public function actionApprove($id)
    {
        $error = true;
        $errorMessage = [];

        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            if ($model['pesanan']->status == 'Diminta') {
                $model['pesanan']->status = 'Disetujui Manager';
                $model['pesanan']->disetujui_manager = Yii::$app->user->identity->id;
                if (!$model['pesanan']->save()) {
                    $errorMessage = array_merge($errorMessage, $model['pesanan']->errors);
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
            } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
                return [
                    "code" => 400,
                    "message" => "Bad Request",
                    "description" => 'Order #' . $id . ' cannot be proceed.',
                ];
            }
            
            $error = false;
            $transaction['pesanan']->commit();
        } catch (\Throwable $e) {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
            $error = true;
            $transaction['pesanan']->rollBack();
        }

        if ($error)
            if ($errorMessage)
                return [
                    "code" => 400,
                    "message" => "Validation Failed",
                    "errors" => $errorMessage,
                ];
            else
                return [
                    "code" => 500,
                    "message" => "Internal Server Error",
                    "description" => "We've faced a problem approving the pesanan, please contact us for assistance.",
                ];
        else
            return [
                "code" => 200,
                "message" => "Pesanan Approved",
                "description" => "Pesanan approved successfully",
            ];
    }

    public function actionReject($id)
    {
        $error = true;
        $errorMessage = [];

        $model['pesanan'] = isset($id) ? $this->findModel($id) : new Pesanan();

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            if ($model['pesanan']->status == 'Diminta') {
                $model['pesanan']->status = 'Ditolak Manager';
                $model['pesanan']->ditolak_manager = Yii::$app->user->identity->id;
                if (!$model['pesanan']->save()) {
                    $errorMessage = array_merge($errorMessage, $model['pesanan']->errors);
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
            } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
                return [
                    "code" => 400,
                    "message" => "Bad Request",
                    "description" => 'Order #' . $id . ' cannot be proceed.',
                ];
            }
            
            $error = false;
            $transaction['pesanan']->commit();
        } catch (\Throwable $e) {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
            $error = true;
            $transaction['pesanan']->rollBack();
        }

        if ($error)
            if ($errorMessage)
                return [
                    "code" => 400,
                    "message" => "Validation Failed",
                    "errors" => $errorMessage,
                ];
            else
                return [
                    "code" => 500,
                    "message" => "Internal Server Error",
                    "description" => "We've faced a problem approving the pesanan, please contact us for assistance.",
                ];
        else
            return [
                "code" => 200,
                "message" => "Pesanan Approved",
                "description" => "Pesanan approved successfully",
            ];
    }

    public function actionListButuhPersetujuan()
    {
        $butuhPersetujuan = Pesanan::find()
            ->select(['pesanan.*', 'unit_kerja.keberangkatan_latitude', 'unit_kerja.keberangkatan_longitude'])
            ->leftJoin('unit_kerja', '`pesanan`.`id_unit_kerja` = `unit_kerja`.`id`')
            ->where(['status' => ['Diminta'], 'id_unit_kerja' => Yii::$app->user->identity->karyawan->id_unit_kerja])
            /*->andWhere(['or',
                   ['disetujui_manager' => Yii::$app->user->identity->id],
                   ['ditolak_manager' => Yii::$app->user->identity->id],
               ])*/
            ->orderBy(['id' => SORT_DESC])
            ->with(['penumpang', 'unitKerja', 'pesananPenumpangs', 'pesananTujuans', 'supir'])
            ->asArray()
            ->all();

        $pesanans = [];
        foreach ($butuhPersetujuan as $key => $pesanan) {
            if ($pesanan) {
                unset($pesanan['penumpang']['password_hash']);
                unset($pesanan['penumpang']['karyawan']['id_user']);
                $pesanans[$key] = $pesanan;
                foreach ($pesanans[$key]['pesananPenumpangs'] as $key1 => $pesananPenumpang) {
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['password_hash']);
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['karyawan']['id_user']);
                }
            }
        }

        if ($pesanans) {
            return [
                "code" => 200,
                "message" => "Data Found",
                "data" => [
                    'Pesanan' => $pesanans,
                ],
            ];
        } else {
            return [
                "code" => 400,
                "message" => "Data Not Found",
            ];
        }
    }

    public function actionListDalamProses()
    {
        $dalamProses = Pesanan::find()
            ->select(['pesanan.*', 'unit_kerja.keberangkatan_latitude', 'unit_kerja.keberangkatan_longitude'])
            ->leftJoin('unit_kerja', '`pesanan`.`id_unit_kerja` = `unit_kerja`.`id`')
            ->where(['status' => ['Disetujui Manager','Dialokasikan Dispatcherf','Disetujui Supervisor','SPK Telah Siap','Checkin','Checkout'], 'id_unit_kerja' => Yii::$app->user->identity->karyawan->id_unit_kerja])
            /*->andWhere(['or',
                   ['disetujui_manager' => Yii::$app->user->identity->id],
                   ['ditolak_manager' => Yii::$app->user->identity->id],
               ])*/
            ->orderBy(['id' => SORT_DESC])
            ->with(['penumpang', 'unitKerja', 'pesananPenumpangs', 'pesananTujuans', 'supir'])
            ->asArray()
            ->all();

        $pesanans = [];
        foreach ($dalamProses as $key => $pesanan) {
            if ($pesanan) {
                unset($pesanan['penumpang']['password_hash']);
                unset($pesanan['penumpang']['karyawan']['id_user']);
                $pesanans[$key] = $pesanan;
                foreach ($pesanans[$key]['pesananPenumpangs'] as $key1 => $pesananPenumpang) {
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['password_hash']);
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['karyawan']['id_user']);
                }
            }
        }

        if ($pesanans) {
            return [
                "code" => 200,
                "message" => "Data Found",
                "data" => [
                    'Pesanan' => $pesanans,
                ],
            ];
        } else {
            return [
                "code" => 400,
                "message" => "Data Not Found",
            ];
        }
    }

    public function actionListSelesai()
    {
        $selesai = Pesanan::find()
            ->select(['pesanan.*', 'unit_kerja.keberangkatan_latitude', 'unit_kerja.keberangkatan_longitude'])
            ->leftJoin('unit_kerja', '`pesanan`.`id_unit_kerja` = `unit_kerja`.`id`')
            ->where(['status' => ['Ditolak Manager','Ditolak Supervisor','SPK Selesai'], 'id_unit_kerja' => Yii::$app->user->identity->karyawan->id_unit_kerja])
            /*->andWhere(['or',
                   ['disetujui_manager' => Yii::$app->user->identity->id],
                   ['ditolak_manager' => Yii::$app->user->identity->id],
               ])*/
            ->orderBy(['id' => SORT_DESC])
            ->with(['penumpang', 'unitKerja', 'pesananPenumpangs', 'pesananTujuans', 'supir'])
            ->asArray()
            ->all();

        $pesanans = [];
        foreach ($selesai as $key => $pesanan) {
            if ($pesanan) {
                unset($pesanan['penumpang']['password_hash']);
                unset($pesanan['penumpang']['karyawan']['id_user']);
                $pesanans[$key] = $pesanan;
                foreach ($pesanans[$key]['pesananPenumpangs'] as $key1 => $pesananPenumpang) {
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['password_hash']);
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['karyawan']['id_user']);
                }
            }
        }

        if ($pesanans) {
            return [
                "code" => 200,
                "message" => "Data Found",
                "data" => [
                    'Pesanan' => $pesanans,
                ],
            ];
        } else {
            return [
                "code" => 400,
                "message" => "Data Not Found",
            ];
        }
    }

    public function actionDetail($id)
    {
        $model['pesanan'] = isset($id) ? function(){
            return Pesanan::find()->select(['pesanan.*', 'unit_kerja.keberangkatan_latitude', 'unit_kerja.keberangkatan_longitude'])->leftJoin('unit_kerja', '`pesanan`.`id_unit_kerja` = `unit_kerja`.`id`')->where(['pesanan.id' => $id])->with(['penumpang', 'unitKerja', 'pesananPenumpangs', 'pesananTujuans', 'supir'])->asArray()->one();
        } : new Pesanan();

        if ($model['pesanan']) {
            return [
                "code" => 200,
                "message" => "Data Found",
                "data" => [
                    'Pesanan' => $model['pesanan'],
                ],
            ];
        } else {
            return [
                "code" => 400,
                "message" => "Data Not Found",
            ];
        }

        return $this->render('detail', [
            'title' => '#' . $model['pesanan']->id,
            'model' => $model,
        ]);
    }
}
