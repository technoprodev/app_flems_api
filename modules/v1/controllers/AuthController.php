<?php

namespace app_flems_api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use app_flems\models\User;

/**
 * Auth controller provides the initial access token that is required for further requests
 * It initially authorizes via Http Basic Auth using a base64 encoded username and password
 */
class AuthController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // Basic Auth accepts Base64 encoded username/password and decodes it for you
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'except' => [
                'options',
                'signup',
                'resend-verification-email',
                'request-reset-password',
                'verify-email',
                'update-password',
                'validate',
            ],
            'auth' => function ($email, $password) {
                $userIdentity = User::findByLogin($email);
                if ($userIdentity && $userIdentity->validatePassword($password))
                    return $userIdentity;
                else
                    return null;
            }
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // Return Header explaining what options are available for next request
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    /**
     * Perform validation on the student account (check if he's allowed login to platform)
     * If everything is alright,
     * Returns the BEARER access token required for futher requests to the API
     * @return array
     */
    public function actionLogin()
    {
        $userIdentity = Yii::$app->user->identity;

        if ($userIdentity->status != 1) {
            return [
                "code" => 400,
                "message" => "Account Unverified",
                "description" => "Please click the verification link sent to you by email to activate your account",
            ];
        }

        $roles = null;
        foreach (\Yii::$app->authManager->getRolesByUser($userIdentity->id) as $key => $role) {
            $roles[] = [
                "name" => $role->name,
                "description" => $role->description,
                "permissions" => \Yii::$app->authManager->getPermissionsByRole($role->name),
            ];
            # code...
        }

        return [
            "code" => 200,
            "message" => "Login Success",
            "data" => [
                "User" => [
                    "id" => $userIdentity->id,
                    "name" => $userIdentity->name,
                    "email" => $userIdentity->email,
                    "token" => $userIdentity->auth_key,
                    "karyawan" => $userIdentity->karyawan,
                    "supir" => $userIdentity->supir,
                ],
                "Roles" => $roles,
            ],
        ];
    }

    /**
     * Signup by student, only firstname, lastname, email and password needed
     * @return array
     */
    public function actionSignup()
    {
        $model['userIdentity'] = new User();
        $model['userIdentity']->scenario = "pass";

        $data = [
            'name' => Yii::$app->request->getBodyParam('name'),
            'mail' => Yii::$app->request->getBodyParam('email'),
            'password' => Yii::$app->request->getBodyParam('password'),
        ];
        $model['userIdentity']->setAttributes($data);

        $transaction['userIdentity'] = User::getDb()->beginTransaction();

        try {
            if (!$model['userIdentity']->save()) {
                if (isset($model['userIdentity']->errors)) {
                    return [
                        "status" => "error",
                        "message" => $model['userIdentity']->errors,
                    ];
                } else {
                    return [
                        "status" => "error",
                        "message" => "We've faced a problem creating your account, please contact us for assistance.",
                    ];
                }
            }
            
            $transaction['userIdentity']->commit();
        } catch (\Exception $e) {
            $render = true;
            $transaction['userIdentity']->rollBack();
        } catch (\Throwable $e) {
            $render = true;
            $transaction['userIdentity']->rollBack();
        }

        return [
            "status" => "success",
            "message" => "Please click on the link sent to you by email to verify your account",
        ];
    }

    /**
     * Re-send manual verification email to student
     * @return array
     */
    public function actionResendVerificationEmail()
    {
        $emailInput = Yii::$app->request->getBodyParam("email");

        $userIdentity = User::findOne([
            'student_email' => $emailInput,
        ]);

        $errors = false;

        if ($userIdentity) {
            if($userIdentity->student_email_verification == User::EMAIL_VERIFIED){
                return [
                    'status' => 'error',
                    'message' => 'You have verified your email'
                ];
            }

            //Check if this user sent an email in past few minutes (to limit email spam)
            $emailLimitDatetime = new \DateTime($userIdentity->student_limit_email);
            date_add($emailLimitDatetime, date_interval_create_from_date_string('2 minutes'));
            $currentDatetime = new \DateTime();

            if ($currentDatetime < $emailLimitDatetime) {
                $difference = $currentDatetime->diff($emailLimitDatetime);
                $minuteDifference = (int) $difference->i;
                $secondDifference = (int) $difference->s;

                $errors = Yii::t('app', "Email was sent previously, you may request another one in {numMinutes, number} minutes and {numSeconds, number} seconds", [
                            'numMinutes' => $minuteDifference,
                            'numSeconds' => $secondDifference,
                ]);
            } else if ($userIdentity->student_email_verification == User::EMAIL_NOT_VERIFIED) {
                $userIdentity->sendVerificationEmail();
            }
        } else {
            $errors['student_email'] = ['Student Account not found'];
        }

        // If errors exist show them
        if($errors){
            return [
                'status' => 'error',
                'message' => $errors
            ];
        }

        // Otherwise return success
        return [
            'status' => 'success',
            'message' => Yii::t('register', 'Please click on the link sent to you by email to verify your account')
        ];
    }

    /**
     * Process email verification
     * @return array
     */
    public function actionVerifyEmail()
    {
        $code = Yii::$app->request->getBodyParam("code");

        if (User::verifyEmail($code)) {
            return [
                'status' => 'success',
                'message' => 'You have verified your email'
            ];
        } else {
            return [
                'status' => 'error',
                'message' => 'Invalid email verification code. Account might already be activated. Please try to login again.'
            ];
        }
    }

    /**
     * Sends password reset email to user
     * @return array
     */
    public function actionRequestResetPassword()
    {
        $emailInput = Yii::$app->request->getBodyParam("email");

        $model['userIdentity'] = new \app_flems\models\PasswordResetRequestForm();
        $model['userIdentity']->email = $emailInput;

        $errors = false;

        if ($model['userIdentity']->validate()) {

            $userIdentity = User::findOne([
                'student_email' => $model['userIdentity']->email,
            ]);

            if ($userIdentity) {
                //Check if this user sent an email in past few minutes (to limit email spam)
                $emailLimitDatetime = new \DateTime($userIdentity->student_limit_email);
                date_add($emailLimitDatetime, date_interval_create_from_date_string('2 minutes'));
                $currentDatetime = new \DateTime();

                if ($currentDatetime < $emailLimitDatetime) {
                    $difference = $currentDatetime->diff($emailLimitDatetime);
                    $minuteDifference = (int) $difference->i;
                    $secondDifference = (int) $difference->s;

                    $errors = Yii::t('app', "Email was sent previously, you may request another one in {numMinutes, number} minutes and {numSeconds, number} seconds", [
                                'numMinutes' => $minuteDifference,
                                'numSeconds' => $secondDifference,
                    ]);

                } else if (!$model['userIdentity']->sendEmail($userIdentity)) {
                    $errors = Yii::t('app', 'Sorry, we are unable to reset password for email provided.');
                }
            }

        } else if (isset($model['userIdentity']->errors['email'])) {
            $errors = $model['userIdentity']->errors['email'];
        }

        // If errors exist show them
        if ($errors) {
            return [
                'status' => 'error',
                'message' => $errors
            ];
        }

        // Otherwise return success
        return [
            'status' => 'success',
            'message' => 'Password reset link sent, please check your email for further instructions.'
        ];
    }

    /**
     * Updates password based on passed token
     * @return array
     */
    public function actionUpdatePassword()
    {
        $token = Yii::$app->request->getBodyParam("token");
        $newPassword = Yii::$app->request->getBodyParam("newPassword");

        $userIdentity =  User::findByPasswordResetToken($token);

        if(!$userIdentity){
            return [
                'status' => 'error',
                'message' => 'Invalid password reset token. Please request another password reset email'
            ];
        }

        if(!$newPassword) {
            return [
                'status' => 'error',
                'message' => 'Password field required'
            ];
        }

        $userIdentity->setPassword($newPassword);
        $userIdentity->removePasswordResetToken();
        $userIdentity->save(false);

        return [
            'status' => 'success',
            'message' => 'Your password has been reset'            
        ];
    }

    /**
     * Validate Google auth id_token sent from mobile
     * after a successful google login
     * @return array
     */
    public function actionValidate()
    {
        $idToken = Yii::$app->request->getBodyParam("id_token");
        $displayName = Yii::$app->request->getBodyParam("displayName");

        // Android and Web Auth Client ID
        $clientId1 = "882152609344-ahm24v4mttplse2ahf35ffe4g0r6noso.apps.googleusercontent.com";
        // iOS Auth Client ID
        $clientId2 = "882152609344-thtlv6jpmuc2ugrmnnfe3g1rb0ba5ess.apps.googleusercontent.com";

        $clientRegular = new \Google_Client(['client_id' => $clientId1]);
        $payload = $clientRegular->verifyIdToken($idToken);
        if(!$payload){
            $clientApple =  new \Google_Client(['client_id' => $clientId2]);
            $payload = $clientApple->verifyIdToken($idToken);
        }

        if ($payload)
        {
            $email = $payload['email'];
            $displayName = $displayName?$displayName:$email;
            $fullname = isset($payload['name'])?$payload['name']:$displayName;

            $existingAgent = Agent::find()->where(['agent_email' => $email])->one();
            if ($existingAgent) {
                //There's already an agent with this email, update his details
                $existingAgent->agent_name = $fullname;
                $existingAgent->agent_email_verified = Agent::EMAIL_VERIFIED;
                $existingAgent->generatePasswordResetToken();

                // On Save, Log him in / Send Access Token
                if ($existingAgent->save()) {
                    Yii::info("[Agent Login Google Native] ".$existingAgent->agent_email, __METHOD__);

                    $accessToken = $existingAgent->accessToken->token_value;
                    return [
                        "status" => 'success',
                        "token" => $accessToken,
                        "agentId" => $existingAgent->agent_id,
                        "name" => $existingAgent->agent_name,
                        "email" => $existingAgent->agent_email
                    ];
                }

                // If Unable to Update
                return [
                    'status' => 'error',
                    'message' => 'Unable to update your account. Please contact us for assistance.'
                ];
            } else {
                //Agent Doesn't have an account, create one for him
                $agent = new Agent([
                    'agent_name' => $fullname,
                    'agent_email' => $email,
                    'agent_email_verified' => Agent::EMAIL_VERIFIED,
                    'agent_limit_email' => new Expression('NOW()')
                ]);
                $agent->setPassword(Yii::$app->security->generateRandomString(6));
                $agent->generateAuthKey();
                $agent->generatePasswordResetToken();

                if ($agent->save()) {
                    //Log agent signup
                    Yii::info("[New Agent Signup Google Native] ".$agent->agent_email, __METHOD__);
                    // Log him in / Send Access Token
                    $accessToken = $agent->accessToken->token_value;
                    return [
                        "status" => 'success',
                        "token" => $accessToken,
                        "agentId" => $agent->agent_id,
                        "name" => $agent->agent_name,
                        "email" => $agent->agent_email
                    ];
                }

                return [
                    'status' => 'error',
                    'message' => 'Unable to create your account. Please contact us for assistance.'
                ];
            }
        }

        // Default Error
        return [
            'status' => 'error',
            'message' => 'Invalid ID token. Please contact us if this issue persists.'
        ];
    }
}
