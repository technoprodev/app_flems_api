<?php
namespace app_flems_api\modules\v1\controllers;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\PesananPenumpang;
use app_flems\models\PesananTujuan;
use technosmart\yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class ProfileController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // remove authentication filter for cors to work
        unset($behaviors['authenticator']);

        // Allow XHR Requests from our different subdomains and dev machines
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // Bearer Auth checks for Authorize: Bearer <Token> header to login the user
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    public function actionDetail()
    {
        $userIdentity = Yii::$app->user->identity;

        if ($userIdentity->status != 1) {
            return [
                "code" => 400,
                "message" => "Account Unverified",
                "description" => "Please click the verification link sent to you by email to activate your account",
            ];
        }

        $roles = null;
        foreach (\Yii::$app->authManager->getRolesByUser($userIdentity->id) as $key => $role) {
            $roles[] = [
                "name" => $role->name,
                "description" => $role->description,
                "permissions" => \Yii::$app->authManager->getPermissionsByRole($role->name),
            ];
            # code...
        }

        return [
            "code" => 200,
            "message" => "Profile Found",
            "data" => [
                "User" => [
                    "id" => $userIdentity->id,
                    "name" => $userIdentity->name,
                    "email" => $userIdentity->email,
                    "phone" => $userIdentity->phone,
                    "token" => $userIdentity->auth_key,
                    "karyawan" => $userIdentity->karyawan,
                    "supir" => $userIdentity->supir,
                ],
            ],
        ];
    }

    public function actionRole()
    {
        $userIdentity = Yii::$app->user->identity;

        if ($userIdentity->status != 1) {
            return [
                "code" => 400,
                "message" => "Account Unverified",
                "description" => "Please click the verification link sent to you by email to activate your account",
            ];
        }

        $roles = null;
        foreach (\Yii::$app->authManager->getRolesByUser($userIdentity->id) as $key => $role) {
            $roles[] = [
                "name" => $role->name,
                "description" => $role->description,
                "permissions" => \Yii::$app->authManager->getPermissionsByRole($role->name),
            ];
            # code...
        }

        return [
            "code" => 200,
            "message" => "Roles Found",
            "data" => [
                "Roles" => $roles,
            ],
        ];
    }
}
