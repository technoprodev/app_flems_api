<?php
namespace app_flems_api\modules\v1\controllers;

use Yii;
use app_flems\models\Pesanan;
use app_flems\models\PesananPenumpang;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class SupirController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => Yii::$app->params['allowedOrigins'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    public static $permissions = [
        ['supir', 'Hak akses supir']
    ];

    /*public function behaviors()
    {
        return [
            'access' => $this->access([
                [[
                    'index',
                ], 'supir'],
            ]),
        ];
    }*/

    protected function findModel($id)
    {
        if (($model = Pesanan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    protected function findModelPesananNew($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'status' => ['Disetujui Supervisor'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => null])->one()) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    protected function findModelPesananActiveCheckin($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'status' => ['SPK Telah Siap'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => 1])->one()) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    protected function findModelPesananActiveCheckout($id)
    {
        if (($model = Pesanan::find()->where(['id' => $id, 'status' => ['Checkin'], 'id_supir' => Yii::$app->user->identity->id])->one()) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    protected function findModelPesananPenumpang($id)
    {
        if (($model = PesananPenumpang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'Order #' . $id . ' cannot be proceed.');
        }
    }

    public function Index()
    {
        $newPesanans = Pesanan::find()->where(['status' => ['Disetujui Supervisor'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => null])->orderBy(['id' => SORT_ASC])->all();
        $activePesanans = Pesanan::find()->where(['status' => ['Disetujui Supervisor', 'SPK Telah Siap', 'Checkin', 'Checkout'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => 1])->orderBy(['id' => SORT_ASC])->all();
        
        return $this->render('index', [
            'newPesanans' => $newPesanans,
            'activePesanans' => $activePesanans,
            'title' => 'Beranda Karyawan',
        ]);
    }

    public function actionListNewPesanan()
    {
        $newPesanans = Pesanan::find()
            ->where(['status' => ['Disetujui Supervisor'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => null])
            ->orderBy(['id' => SORT_ASC])
            ->with(['penumpang', 'pesananPenumpangs', 'pesananTujuans'])
            ->asArray()
            ->all();

        $pesanans = [];
        foreach ($newPesanans as $key => $pesanan) {
            if ($pesanan) {
                unset($pesanan['penumpang']['password_hash']);
                unset($pesanan['penumpang']['karyawan']['id_user']);
                $pesanans[$key] = $pesanan;
                foreach ($pesanans[$key]['pesananPenumpangs'] as $key1 => $pesananPenumpang) {
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['password_hash']);
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['karyawan']['id_user']);
                }
            }
        }

        if ($pesanans) {
            return [
                "code" => 200,
                "message" => "Data Found",
                "data" => [
                    'Pesanan' => $pesanans,
                ],
            ];
        } else {
            return [
                "code" => 400,
                "message" => "Data Not Found",
            ];
        }
    }

    public function actionListActivePesanan()
    {
        $activePesanans = Pesanan::find()
            ->where(['status' => ['Disetujui Supervisor', 'SPK Telah Siap', 'Checkin', 'Checkout'], 'id_supir' => Yii::$app->user->identity->id, 'supir_konfirmasi' => 1])
            ->orderBy(['id' => SORT_ASC])
            ->with(['penumpang', 'pesananPenumpangs', 'pesananTujuans'])
            ->asArray()
            ->all();

        $pesanans = [];
        foreach ($activePesanans as $key => $pesanan) {
            if ($pesanan) {
                unset($pesanan['penumpang']['password_hash']);
                unset($pesanan['penumpang']['karyawan']['id_user']);
                $pesanans[$key] = $pesanan;
                foreach ($pesanans[$key]['pesananPenumpangs'] as $key1 => $pesananPenumpang) {
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['password_hash']);
                    unset($pesanans[$key]['pesananPenumpangs'][$key1]['karyawan']['id_user']);
                }
            }
        }
        
        if ($pesanans) {
            return [
                "code" => 200,
                "message" => "Data Found",
                "data" => [
                    'Pesanan' => $pesanans,
                ],
            ];
        } else {
            return [
                "code" => 400,
                "message" => "Data Not Found",
            ];
        }
    }

    public function actionKonfirmasi($id)
    {
        $error = false;
        $errorMessage = [];

        $model['pesanan'] = isset($id) ? $this->findModelPesananNew($id) : new Pesanan();

        $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

        try {
            $model['pesanan']->supir_konfirmasi = 1;
            if (!$model['pesanan']->save()) {
                $errorMessage = array_merge($errorMessage, $model['pesanan']->errors);
                throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
            }
            
            $transaction['pesanan']->commit();
            Yii::$app->session->setFlash('success', 'Data has been saved.');
        } catch (\Exception $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
        } catch (\Throwable $e) {
            $error = true;
            $transaction['pesanan']->rollBack();
        }

        if ($error)
            if ($errorMessage)
                return [
                    "code" => 400,
                    "message" => "Validation Failed",
                    "errors" => $errorMessage,
                ];
            else
                return [
                    "code" => 500,
                    "message" => "Internal Server Error",
                    "description" => "We've faced a problem updating the pesanan, please contact us for assistance.",
                ];
        else
            return [
                "code" => 200,
                "message" => "Data Updated",
                "description" => "Pesanan updated successfully",
            ];

        if ($error)
            throw new \yii\web\HttpException(400, 'We are sorry, but we cannot proceed your action. Please try again.');
        else
            return $this->redirect(['index']);
    }

    public function actionFormCheckin($id)
    {
        $error = false;
        $errorMessage = [];

        $model['pesanan'] = isset($id) ? $this->findModelPesananActiveCheckin($id) : new Pesanan();

        if (true) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                $model['pesanan']->status = 'Checkin';
                $model['pesanan']->waktu_checkin = new \yii\db\Expression("now()");
                if (!$model['pesanan']->save()) {
                    $errorMessage = array_merge($errorMessage, $model['pesanan']->errors);
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Submission successful.');
            } catch (\Exception $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            $render = true;
        }

        if ($error)
            if ($errorMessage)
                return [
                    "code" => 400,
                    "message" => "Validation Failed",
                    "errors" => $errorMessage,
                ];
            else
                return [
                    "code" => 500,
                    "message" => "Internal Server Error",
                    "description" => "We've faced a problem updating the pesanan, please contact us for assistance.",
                ];
        else
            return [
                "code" => 200,
                "message" => "Data Updated",
                "description" => "Pesanan updated successfully",
            ];

        if ($render)
            return $this->render('form-checkin', [
                'model' => $model,
                'title' => 'Checkin #' . $model['pesanan']->id,
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionFormCheckout($id)
    {
        $error = false;
        $errorMessage = [];

        $model['pesanan'] = isset($id) ? $this->findModelPesananActiveCheckout($id) : new Pesanan();

        if (true) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                $model['pesanan']->status = 'Checkout';
                $model['pesanan']->waktu_checkout = new \yii\db\Expression("now()");
                if (!$model['pesanan']->save()) {
                    $errorMessage = array_merge($errorMessage, $model['pesanan']->errors);
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Submission successful.');
            } catch (\Exception $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
            }
        } else {
            foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            $render = true;
        }

        if ($error)
            if ($errorMessage)
                return [
                    "code" => 400,
                    "message" => "Validation Failed",
                    "errors" => $errorMessage,
                ];
            else
                return [
                    "code" => 500,
                    "message" => "Internal Server Error",
                    "description" => "We've faced a problem updating the pesanan, please contact us for assistance.",
                ];
        else
            return [
                "code" => 200,
                "message" => "Data Updated",
                "description" => "Pesanan updated successfully",
            ];

        if ($render)
            return $this->render('form-checkout', [
                'model' => $model,
                'title' => 'Checkout #' . $model['pesanan']->id,
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionSendLocation($id)
    {
        $error = false;
        $errorMessage = [];

        $model['pesanan'] = isset($id) ? $this->findModelPesananActiveCheckout($id) : new Pesanan();

        if (false) {
            $post = Yii::$app->request->post();

            $model['pesanan']->load($post);

            $transaction['pesanan'] = Pesanan::getDb()->beginTransaction();

            try {
                $model['pesanan']->status = 'Checkout';
                $model['pesanan']->waktu_checkout = new \yii\db\Expression("now()");
                if (!$model['pesanan']->save()) {
                    $errorMessage = array_merge($errorMessage, $model['pesanan']->errors);
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['pesanan']->commit();
                Yii::$app->session->setFlash('success', 'Submission successful.');
            } catch (\Exception $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pesanan']->rollBack();
            }
        } else {
            /*foreach ($model['pesanan']->pesananPenumpangs as $key => $pesananPenumpang)
                $model['pesanan_penumpang'][] = $pesananPenumpang;

            foreach ($model['pesanan']->pesananTujuans as $key => $pesananTujuan)
                $model['pesanan_tujuan'][] = $pesananTujuan;

            $render = true;*/
        }

        if ($error)
            if ($errorMessage)
                return [
                    "code" => 400,
                    "message" => "Validation Failed",
                    "errors" => $errorMessage,
                ];
            else
                return [
                    "code" => 500,
                    "message" => "Internal Server Error",
                    "description" => "We've faced a problem updating the pesanan, please contact us for assistance.",
                ];
        else
            return [
                "code" => 200,
                "message" => "Data Updated",
                "description" => "Pesanan updated successfully",
            ];

        if ($render)
            return $this->render('form-checkout', [
                'model' => $model,
                'title' => 'Checkout #' . $model['pesanan']->id,
            ]);
        else
            return $this->redirect(['index']);
    }
}
