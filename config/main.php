<?php
$params['user.passwordResetTokenExpire'] = 3600;
$params['allowedOrigins'] = ['*'];

$config = [
    'id' => 'app_flems_api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_flems_api\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app_flems_api/modules/v1',
            'class' => 'app_flems_api\modules\v1\Module',
        ],
    ],
    'components' => [
        'request' => [
            // 'csrfParam' => '_csrf-app_flems_api',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityClass' => 'app_flems\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'loginUrl' => null
            // 'identityCookie' => ['name' => '_identity-app_flems_api', 'httpOnly' => true],
        ],
        /*'session' => [
            'name' => 'session-app_flems_api',
        ],*/
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                // ddx($response->data);
                if ($response->data !== null && array_key_exists('type', $response->data)) {
                    $response->data = [
                        'code' => isset($response->data['status']) ? $response->data['status'] : 500,
                        'message' => $response->data['name'],
                        'description' => $response->data['message'],
                    ];
                    $response->statusCode = 200;
                }
            },
        ],
        /*'errorHandler' => [
            'errorAction' => 'site/error',
        ],*/
        /*'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                [ // AuthController
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/auth',
                    'pluralize' => false,
                    'patterns' => [
                        'GET login' => 'login',
                        'POST signup' => 'signup',
                        'POST resend-verification-email' => 'resend-verification-email',
                        'POST request-reset-password' => 'request-reset-password',
                        'PATCH verify' => 'verify-email',
                        'PATCH update-password' => 'update-password',
                        'OPTIONS' => 'options',
                    ]
                ],
                [ // DriverController
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/driver',
                    'pluralize' => false,
                    'patterns' => [
                        'POST,HEAD index' => 'index',
                        'POST create' => 'create',
                        'PUT,PATCH update/<id>' => 'update',
                        'DELETE delete/<id>' => 'delete',
                        'OPTIONS' => 'options',
                    ]
                ],
                [ // Employee Dashboard Controller
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/employee/dashboard',
                    'pluralize' => false,
                    'patterns' => [
                        'POST,HEAD index' => 'index',
                        'POST create' => 'create',
                        'PUT,PATCH update/<id>' => 'update',
                        'DELETE delete/<id>' => 'delete',
                        'OPTIONS' => 'options',
                    ]
                ],
            ],
        ],*/
    ],
    'params' => $params,
];

return $config;